------- Run this against the Access DB copy in SQL to get a tmp table with the state strings for each demographic then...
Select 
[State/Estado] AS StateString, ID
INTO tmpAccessImportDemographicState
FROM demographics 
 WHERE ([Last Name/Apellido] IS NOT NULL AND LTrim(RTrim([Last Name/Apellido])) <> '')
 OR ([First Name/ Nombre] IS NOT NULL AND LTrim(RTrim([First Name/ Nombre])) <> '')
 AND [State/Estado] <> 'Delaware' AND [State/Estado] IS NOT NULL AND ltrim(rtrim([State/Estado])) <> ''

 ------- Run this against the Access DB copy in SQL to get a bunch of update statements then run the update statements against the app db
 Select 'update demographic set StateID = ' + 
CASE StateString 
	WHEN 'D.C.' THEN '3'
	WHEN 'DC' THEN '3'
	WHEN 'District of Columbia' THEN '3'
	WHEN 'Washington' THEN '3'
	WHEN 'Washington DC' then '3'
	WHEN 'Washington, DC' then '3'
	WHEN 'MD' THEN '1'
	WHEN 'MdMD' THEN '1'
	WHEN 'Md
MD' THEN '1' --you saw that right. someone put a line break in the data
	WHEN 'Maryland' THEN '1'
	WHEN '20910' THEN '1'
	WHEN 'Virginia' THEN '2'
	WHEN 'Delaware' THEN '118'
	ELSE 'NULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULLNULL'
	--that will help us find cases we're not handling above
	END
	+
	' where barcodeId = ' + CONVERT(varchar(255),ID)
FROM tmpAccessImportDemographicState tmp
WHERE statestring IS NOT NULL
