--select everything into a working table for ease of movement
select zManna.ID AS BarcodeID
,CASE
	WHEN Gender Like '%ema%' THEN 39
	WHEN Gender LIKE 'mal%' THEN 38
	ELSE NULL
END AS GenderID
,CASE
	WHEN EmployedStatus IS NULL THEN NULL
	WHEN EmployedStatus = 'Permanent' THEN 40
	WHEN EmployedStatus = 'Seasonal' THEN 42
	WHEN EmployedStatus = 'Temporary' THEN 41
	ELSE NULL 
END AS EmploymentStatusID
,CASE
	WHEN Race IS NULL THEN NULL
	WHEN Race = 'American Indian or Alaska Native' THEN 44
	WHEN Race = 'Asian' THEN 45
	WHEN Race = 'Black or African American' THEN 46
	WHEN Race = 'Don�t know' THEN 49
	WHEN Race = 'White' THEN 48
END AS RaceID
,CASE
	WHEN Transportation IS NULL THEN NULL
	WHEN Transportation = 'Bus' THEN 54
	WHEN Transportation = 'car' THEN 59
	WHEN Transportation = 'Friend/Family' THEN 53
	WHEN Transportation = 'Own Vehicle' THEN 51
	WHEN Transportation = 'Own Vehicle Ride' THEN 59
	WHEN Transportation = 'Ride' THEN 52
	WHEN Transportation = 'Taxi' THEN 58
	WHEN Transportation = 'Walk' THEN 57
END AS TransportationID
,CASE 
	WHEN BabyFoodStage IS NULL THEN NULL
	WHEN BabyFoodStage = '1' THEN 35
	WHEN BabyFoodStage = '1/T' THEN NULL
	WHEN BabyFoodStage = '2/3' THEN 36
	WHEN BabyFoodStage = 'T' THEN 37
END AS BabyFoodStageID
,Children AS NumberOfChildren
,EmailAddress
,Diabetic AS NeedsDiabeticBox
,Vegetarian AS NeedsVegetarianBox
,FormulaType
,Employed AS IsEmployed
,MonthlyGross AS MonthlyGrossDollars
,YearlyGross AS YearlyGrossDollars
,FoodStamp AS FoodStampDollars
,Unemployment AS UnemploymentDollars
,Medicaid AS IsUsingMedicaid
,SSI AS SocialSecurityInsuranceDollars
,SSDI AS SocialSecurityDisabilityInsuranceDollars
,EnergyAssistance AS IsReceivingEnergyAssistance
,TANF AS TANFDollars
,Other AS OtherDollars
,PrimaryLanguage
,EmergencyContactName
,Phone AS EmergencyContactPhone
INTO #tmpMannaConversion
FROM [SCAccessCopies].dbo.Manna AS zManna
WHERE EXISTS (select Demographic.BarcodeID FROM Demographic WHERE zManna.ID = Demographic.BarcodeID)

--insert from the working table
INSERT INTO MannaData (DemographicID, GenderID, EmploymentStatusID, RaceID, TransportationID, BabyFoodStageID
, NumberOfChildren, EmailAddress, NeedsDiabeticBox, NeedsVegetarianBox, FormulaType, IsEmployed, MonthlyGrossDollars
, YearlyGrossDollars, FoodStampDollars, UnemploymentDollars, IsUsingMedicaid, SocialSecurityInsuranceDollars, SocialSecurityDisabilityInsuranceDollars
, IsReceivingEnergyAssistance, TANFDollars, OtherDollars, PrimaryLanguage, EmergencyContactName, EmergencyContactPhone)
select d.DemographicID, GenderID, EmploymentStatusID, RaceID, TransportationID, BabyFoodStageID
, NumberOfChildren, EmailAddress, NeedsDiabeticBox, NeedsVegetarianBox, FormulaType, IsEmployed, MonthlyGrossDollars
, YearlyGrossDollars, FoodStampDollars, UnemploymentDollars, IsUsingMedicaid, SocialSecurityInsuranceDollars, SocialSecurityDisabilityInsuranceDollars
, IsReceivingEnergyAssistance, TANFDollars, OtherDollars, PrimaryLanguage, EmergencyContactName, EmergencyContactPhone
from #tmpMannaConversion AS tmp
INNER JOIN Demographic AS d ON d.BarcodeId = tmp.BarcodeID

--select transportation FROM [SCAccessCopies].dbo.Manna group by transportation

--SELECT BabyFoodStage FROM [SCAccessCopies].dbo.Manna AS zManna group by BabyFoodStage
--select * from LookupCategory
--select * from LookupValue where LookupCategoryID = 10
