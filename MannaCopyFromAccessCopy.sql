--SET IDENTITY_INSERT StCamillus.dbo.MannaData OFF

SELECT manna.MannaID AS MannaDataID, ID AS DemographicID, 

CASE manna.Gender
	WHEN 'female' THEN 39
	WHEN 'male' THEN 38
	ELSE NULL
	END AS GenderID,

CASE manna.EmployedStatus
	WHEN 'Permanent' THEN 40
	WHEN 'Seasonal' THEN 42
	WHEN 'Temporary' THEN 41
	ELSE NULL
	END AS EmploymentStatusID,

CASE manna.Race
	WHEN 'American Indian or Alaska Native' THEN 44
	WHEN 'Asian' THEN 45
	WHEN 'Black or African American' THEN 46
	WHEN 'Don�t know' THEN 49
	WHEN 'White' THEN 48
	ELSE NULL
	END AS RaceID,

CASE manna.Transportation
	WHEN 'Bus' THEN 54
	WHEN 'Friend/Family' THEN 53
	WHEN 'Own Vehicle' THEN 51
	WHEN 'Own Vehicle Ride' THEN 51
	WHEN 'Ride' THEN 52
	WHEN 'Taxi' THEN 58
	WHEN 'Walk' THEN 57
	ELSE NULL
	END AS TransportationID,

CASE manna.BabyFoodStage
	WHEN '1' THEN 35
	WHEN '2/3' THEN 36
	WHEN 'T' THEN 37
	ELSE NULL
	END AS BabyFoodStageID

FROM SCAccessCopies.dbo.Manna manna


select manna.BabyFoodStage, count(1) from SCAccessCopies.dbo.Manna manna group by manna.BabyFoodStage

SELECT count(*) from SCAccessCopies.dbo.Manna WHERE mannaID is NOT NULL

--SET IDENTITY_INSERT StCamillus.dbo.MannaData ON
