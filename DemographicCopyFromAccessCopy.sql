--Probably easier to select into a temp table so that we can easily query any problem data from that table
--See the zDemo FROM statement to correct the name of your DB that has the Access Data copy
--Was able to map everything from original Demographics table to new Demographic table except for: 
--Logo ID, Age, Need work, Need insufficient $$, need Health problems AND Bi-weekly incomePantry

INSERT INTO Demographic
(BarcodeID, LastName, FirstName, StreetAddress, City,
StateID, CountyID, ZipCode, PhoneNumber,
OriginCountryID, EthnicityID, MarriageStatusID,
TotalFamilyMembers, Adults, ChildrenZeroToTwo,
ChildrenThreeToFive, ChildrenSixToFourteen, ChildrenFifteenToEighteen,
Note, SelfDisclosure, Children, BiWeeklyIncomeID)

SELECT 
zDemo.ID AS BarcodeId, 

COALESCE(zDemo.[Last Name/Apellido], '')
--CASE zDemo.[Last Name/Apellido]
--	WHEN NULL THEN ''
--	ELSE zDemo.[Last Name/Apellido] END
 AS LastName, 

COALESCE(zDemo.[First Name/ Nombre], '')
--CASE zDemo.[First Name/ Nombre]
--	WHEN NULL THEN ''
--	ELSE zDemo.[First Name/ Nombre] END
 AS FirstName, 

zDemo.[Address/Direccion] AS StreetAddress, zDemo.[City/Ciudad] AS City,

CASE zDemo.[State/Estado] 
	WHEN 'MD' THEN 1
	WHEN 'Maryland' THEN 1
	WHEN 'Md
MD' THEN 1 --yes, there's a line break in that one.
	WHEN '20910' THEN 1
	WHEN 'Virginia' THEN 2
	WHEN 'Washington DC' THEN 3
	WHEN 'Washington, DC' THEN 2
	WHEN 'DC' THEN 2
	WHEN 'D.C.' THEN 2
	WHEN 'District of Columbia' THEN 2
	WHEN 'Washington' THEN 2
	WHEN 'Delaware' THEN 118
	ELSE NULL
	END AS StateID,

CASE zDemo.[County/ Condado]
	WHEN 'Montgomery' THEN 4
	WHEN 'Prince George' THEN 5
	WHEN 'Howard' THEN 6
	WHEN 'Baltimore' THEN 119
	WHEN 'Washington, DC' THEN 120
	WHEN 'DC' THEN 120
	WHEN 'Anne Arundel' THEN 121
	WHEN 'Kent' THEN 122
	WHEN 'Arlington' THEN 123
	WHEN 'Worcester' THEN 124
	WHEN 'Somerset' THEN 125
	ELSE NULL 
	END AS CountyID,

zDemo.[Zip Code/ Codigo Postal] AS ZipCode, zDemo.[PhoneNumber/ Telefono] AS PhoneNumber,

CASE ltrim(rtrim(zDemo.[Country of Origin/ Pais de Origen]))
	WHEN 'St. Vincent' THEN 126
	WHEN 'Brazil' THEN 68
	WHEN 'Estonia' THEN 127
	WHEN 'Rep Dominicana' THEN 76
	WHEN 'Belize' THEN 128
	WHEN 'Phillipines' THEN 129
	WHEN '' THEN NULL
	WHEN 'Uruguay' THEN 130
	WHEN 'Poland' THEN 131
	WHEN 'Gabon' THEN 132
	WHEN 'Grenada' THEN 133
	WHEN 'US' THEN 106
	WHEN 'Central African Rep' THEN 134
	WHEN NULL THEN NULL
	ELSE (SELECT LookupValueID FROM [StCamillus].dbo.LookupValue WHERE [Description] = zDemo.[Country of Origin/ Pais de Origen])
	END AS CountryID, 

CASE ltrim(rtrim(zDemo.[Ethnicity/Raza]))
	WHEN 'African American
African American' THEN 8 --yes, there's a line break in that one.
	WHEN 'African-American' THEN 8
	WHEN 'African America' THEN 8
	WHEN 'Black/African American' THEN 8
	WHEN 'White/Americans' THEN 7
	WHEN 'White' THEN 7
	WHEN 'White American' THEN 7
	WHEN 'Hiapanic' THEN 10
	WHEN 'Hispanic/American' THEN 10
	WHEN 'Latino' THEN 10
	WHEN 'Hispanic/Americans' THEN 10
	WHEN 'latino
latino' THEN 10 --yes, there's a line break in that one.
	WHEN 'Hspanic' THEN 10
	WHEN 'hispanic/latin' THEN 10
	WHEN 'Hispanic
Hispanic' THEN 10 --yes, there's a line break in that one.
	WHEN 'Hispanic/Latino' THEN 10
	WHEN 'Hispan' THEN 10
	WHEN 'Hispano' THEN 10
	WHEN 'Latin' THEN 10
	WHEN 'hisoanic' THEN 10
	WHEN 'Latina' THEN 10
	WHEN 'Hispanioc' THEN 10
	WHEN '' THEN NULL
	ELSE NULL
	END AS EthnicityID,

CASE ltrim(rtrim(zDemo.[Marriage status]))
	WHEN 'Accompanied' THEN 135
	WHEN '' THEN NULL
	WHEN NULL THEN NULL
	ELSE (SELECT LookupValueID FROM [StCamillus].dbo.LookupValue WHERE [Description] = zDemo.[Marriage status] AND LookupCategoryID = 5)
	END AS MarriageStatusID,

zDemo.[Number of People in Family/Numero de personas en Familia] AS TotaFamilyMembers,
zDemo.[Adults/Adultos] AS Adults, zDemo.[0-2 years/anos] AS ChildrenZeroToTwo,
zDemo.[3-5 years/anos] AS ChildrenThreeToFive, zDemo.[6-14 years/anos] AS ChildrenSixToFourteen,
zDemo.[15-18 years/anos] AS ChildrenFifteenToEighteen, zDemo.[Note], zDemo.SelfDisclosure,
zDemo.[Children/Ninos] AS Children,

CASE zDemo.[Bi-weekly Income]
	WHEN '0-200' THEN 27
	WHEN '300' THEN 28
	WHEN '201-400' THEN 28
	WHEN '401-600' THEN 29
	WHEN '601-800' THEN 30
	WHEN '801-1,000' THEN 31
	WHEN '1,001-1,200' THEN 32
	WHEN '1,201-1,400' THEN 33
	WHEN '1,401-1,600' THEN 34
	ELSE NULL 
	END AS BiWeeklyIncomeID
	FROM [PantryAccessImport].dbo.Demographics AS zDemo 
--ORDER BY zDemo.ID
--ORDER BY CountryID DESC


--select * from StCamillus.dbo.lookupValue where LookupCategoryID = 5

--select * from StCamillus.Dbo.LookupCategory

--SELECT zDemo.[Marriage status], count(1)
--FROM [SCAccessCopies].dbo.Demographics AS zDemo
--WHERE NOT EXISTS (SELECT [Description] FROM [StCamillus].dbo.LookupValue WHERE [Description] = zDemo.[Marriage status] AND LookupCategoryID = 5)
--GROUP BY zDemo.[Marriage status]

--insert into LookupValue (LookupCategoryID, Description)
--VALUES (5, 'Accompanied')

--SELECT zDemo.[Bi-weekly Income], count(1)
--FROM [SCAccessCopies].dbo.Demographics AS zDemo
--WHERE NOT EXISTS (SELECT [Description] FROM [StCamillus].dbo.LookupValue WHERE [Description] = zDemo.[Bi-weekly Income] AND LookupCategoryID = 9)
--GROUP BY zDemo.[Bi-weekly Income]


--select [County/ Condado], count(ID) FROM [SCAccessCopies].dbo.Demographics GROUP BY [County/ Condado]

--select zDemo.[State/Estado] 
--FROM [SCAccessCopies].dbo.Demographics zDemo
--GROUP BY zDemo.[State/Estado]