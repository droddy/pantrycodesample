/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2012 (11.0.6251)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

--USE [SQL2012_949735_food]
GO
/****** Object:  Table [dbo].[Demographic]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Demographic](
	[DemographicID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[StreetAddress] [nvarchar](255) NULL,
	[City] [nvarchar](100) NULL,
	[StateID] [int] NULL,
	[CountyID] [int] NULL,
	[ZipCode] [nvarchar](15) NULL,
	[PhoneNumber] [nvarchar](20) NULL,
	[OriginCountryID] [int] NULL,
	[EthnicityID] [int] NULL,
	[MarriageStatusID] [int] NULL,
	[TotalFamilyMembers] [smallint] NULL,
	[Adults] [smallint] NULL,
	[ChildrenZeroToTwo] [bit] NOT NULL,
	[ChildrenThreeToFive] [bit] NOT NULL,
	[ChildrenSixToFourteen] [bit] NOT NULL,
	[ChildrenFifteenToEighteen] [bit] NOT NULL,
	[Note] [nvarchar](max) NULL,
	[SelfDisclosure] [bit] NOT NULL,
	[Children] [smallint] NULL,
	[DOB] [date] NULL,
	[Age]  AS (floor(datediff(day,[DOB],getdate())/(365.25))),
	[BiWeeklyIncomeID] [int] NULL,
	[BarcodeId] [int] NOT NULL,
 CONSTRAINT [PK_Demographic] PRIMARY KEY CLUSTERED 
(
	[DemographicID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DemographicMultiValue]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DemographicMultiValue](
	[DemographicMultiValueID] [int] IDENTITY(1,1) NOT NULL,
	[DemographicID] [int] NOT NULL,
	[LookupValueID] [int] NOT NULL,
 CONSTRAINT [PK_DemographicMultiValue] PRIMARY KEY CLUSTERED 
(
	[DemographicMultiValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Distribution]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distribution](
	[DistributionID] [int] IDENTITY(1,1) NOT NULL,
	[DemographicID] [int] NOT NULL,
	[DistributionDate] [date] NOT NULL,
	[BagCount] [smallint] NOT NULL,
	[GaveInfantFormula] [bit] NOT NULL,
	[PantryLocationID] [int] NOT NULL,
 CONSTRAINT [PK_Distribution] PRIMARY KEY CLUSTERED 
(
	[DistributionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupCategory]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupCategory](
	[LookupCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_LookupCategory] PRIMARY KEY CLUSTERED 
(
	[LookupCategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LookupValue]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LookupValue](
	[LookupValueID] [int] IDENTITY(1,1) NOT NULL,
	[LookupCategoryID] [int] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Visible] [bit] NOT NULL,
	[SortOrder] [smallint] NULL,
 CONSTRAINT [PK_LookupValue] PRIMARY KEY CLUSTERED 
(
	[LookupValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MannaData]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MannaData](
	[MannaDataID] [int] IDENTITY(1,1) NOT NULL,
	[DemographicID] [int] NOT NULL,
	[GenderID] [int] NULL,
	[EmploymentStatusID] [int] NULL,
	[RaceID] [int] NULL,
	[TransportationID] [int] NULL,
	[BabyFoodStageID] [int] NULL,
	[EmailAddress] [nvarchar](100) NULL,
	[NumberOfChildren] [smallint] NULL,
	[NeedsDiabeticBox] [bit] NOT NULL,
	[NeedsVegetarianBox] [bit] NOT NULL,
	[FormulaType] [nvarchar](100) NULL,
	[IsEmployed] [bit] NOT NULL,
	[MonthlyGrossDollars] [money] NULL,
	[YearlyGrossDollars] [money] NULL,
	[FoodStampDollars] [money] NULL,
	[UnemploymentDollars] [money] NULL,
	[IsUsingMedicaid] [bit] NOT NULL,
	[SocialSecurityInsuranceDollars] [money] NULL,
	[SocialSecurityDisabilityInsuranceDollars] [money] NULL,
	[IsReceivingEnergyAssistance] [bit] NOT NULL,
	[TANFDollars] [money] NULL,
	[OtherDollars] [money] NULL,
	[PrimaryLanguage] [nvarchar](50) NULL,
	[EmergencyContactName] [nvarchar](100) NULL,
	[EmergencyContactPhone] [nvarchar](100) NULL,
 CONSTRAINT [PK_MannaData] PRIMARY KEY CLUSTERED 
(
	[MannaDataID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MannaPickup]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MannaPickup](
	[MannaPickupId] [int] IDENTITY(-2000000000,1) NOT NULL,
	[MannaDataId] [int] NOT NULL,
	[RequestedDate] [datetime] NOT NULL,
	[PickupDate] [datetime] NOT NULL,
	[PickupLocationId] [int] NOT NULL,
	[Notes] [nvarchar](max) NULL,
 CONSTRAINT [PK_MannaPickup] PRIMARY KEY CLUSTERED 
(
	[MannaPickupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[Ethnicity_Manna]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Ethnicity_Manna]
AS
SELECT        CASE [Description] WHEN 'Hispanic' THEN 'Hispanic/Latino' ELSE 'Not Hispanic/Latino' END AS MannaDescription, LookupValueID, LookupCategoryID, Description, Visible, SortOrder
FROM            dbo.LookupValue
WHERE        (LookupCategoryID = 4)
GO
/****** Object:  View [dbo].[vwDemographics]    Script Date: 9/4/2017 3:09:55 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW[dbo].[vwDemographics]
                AS
                SELECT dbo.Demographic.DemographicID, dbo.Demographic.LastName, dbo.Demographic.FirstName, dbo.Demographic.StreetAddress, dbo.Demographic.City, LookupState.Description AS State, LookupCounty.Description AS County, LookupCountry.Description AS Country, EthnicityLookup.Description AS Ethnicity,
                         MarriageStatusLookup.Description AS[Marriage Status], IncomeLookup.Description AS[Bi - Weekly Income]
                FROM  dbo.Demographic INNER JOIN
                         dbo.LookupValue AS LookupState ON dbo.Demographic.StateID = LookupState.LookupValueID INNER JOIN
                         dbo.LookupValue AS LookupCounty ON dbo.Demographic.CountyID = LookupCounty.LookupValueID INNER JOIN
                         dbo.LookupValue AS LookupCountry ON dbo.Demographic.OriginCountryID = LookupCountry.LookupValueID INNER JOIN
                         dbo.LookupValue AS EthnicityLookup ON dbo.Demographic.EthnicityID = EthnicityLookup.LookupValueID INNER JOIN
                         dbo.LookupValue AS MarriageStatusLookup ON dbo.Demographic.MarriageStatusID = MarriageStatusLookup.LookupValueID INNER JOIN
                         dbo.LookupValue AS IncomeLookup ON dbo.Demographic.BiWeeklyIncomeID = IncomeLookup.LookupValueID

GO
ALTER TABLE [dbo].[Demographic] ADD  CONSTRAINT [DF_Demographic_ChildrenZeroToTwo]  DEFAULT ((0)) FOR [ChildrenZeroToTwo]
GO
ALTER TABLE [dbo].[Demographic] ADD  CONSTRAINT [DF_Demographic_ChildrenThreeToFive]  DEFAULT ((0)) FOR [ChildrenThreeToFive]
GO
ALTER TABLE [dbo].[Demographic] ADD  CONSTRAINT [DF_Demographic_ChildrenSixToFourteen]  DEFAULT ((0)) FOR [ChildrenSixToFourteen]
GO
ALTER TABLE [dbo].[Demographic] ADD  CONSTRAINT [DF_Demographic_ChildrenFifteenToEighteen]  DEFAULT ((0)) FOR [ChildrenFifteenToEighteen]
GO
ALTER TABLE [dbo].[LookupValue] ADD  CONSTRAINT [DF_LookupValue_Visible]  DEFAULT ((1)) FOR [Visible]
GO
ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_NeedsDiabeticBox]  DEFAULT ((0)) FOR [NeedsDiabeticBox]
GO
ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_NeedsVegetarianBox]  DEFAULT ((0)) FOR [NeedsVegetarianBox]
GO
ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_IsEmployed]  DEFAULT ((0)) FOR [IsEmployed]
GO
ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_IsUsingMedicaid]  DEFAULT ((0)) FOR [IsUsingMedicaid]
GO
ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_IsReceivingEnergyAssistance]  DEFAULT ((0)) FOR [IsReceivingEnergyAssistance]
GO
ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_BiWeeklyIncomeID] FOREIGN KEY([BiWeeklyIncomeID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_BiWeeklyIncomeID]
GO
ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_CountyID] FOREIGN KEY([CountyID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_CountyID]
GO
ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_EthnicityID] FOREIGN KEY([EthnicityID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_EthnicityID]
GO
ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_MarriageStatusID] FOREIGN KEY([MarriageStatusID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_MarriageStatusID]
GO
ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_OriginCountryID] FOREIGN KEY([OriginCountryID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_OriginCountryID]
GO
ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_StateID] FOREIGN KEY([StateID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_StateID]
GO
ALTER TABLE [dbo].[DemographicMultiValue]  WITH CHECK ADD  CONSTRAINT [FK_DemographicMultiValue_Demographic_DemographicID] FOREIGN KEY([DemographicID])
REFERENCES [dbo].[Demographic] ([DemographicID])
GO
ALTER TABLE [dbo].[DemographicMultiValue] CHECK CONSTRAINT [FK_DemographicMultiValue_Demographic_DemographicID]
GO
ALTER TABLE [dbo].[DemographicMultiValue]  WITH CHECK ADD  CONSTRAINT [FK_DemographicMultiValue_LookupValue_LookupValueID] FOREIGN KEY([LookupValueID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[DemographicMultiValue] CHECK CONSTRAINT [FK_DemographicMultiValue_LookupValue_LookupValueID]
GO
ALTER TABLE [dbo].[Distribution]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_Demographic_DemographicID] FOREIGN KEY([DemographicID])
REFERENCES [dbo].[Demographic] ([DemographicID])
GO
ALTER TABLE [dbo].[Distribution] CHECK CONSTRAINT [FK_Distribution_Demographic_DemographicID]
GO
ALTER TABLE [dbo].[Distribution]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_LookupValue_PantryLocationID] FOREIGN KEY([PantryLocationID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[Distribution] CHECK CONSTRAINT [FK_Distribution_LookupValue_PantryLocationID]
GO
ALTER TABLE [dbo].[LookupValue]  WITH CHECK ADD  CONSTRAINT [FK_LookupValue_LookupCategory_LookupCategoryID] FOREIGN KEY([LookupCategoryID])
REFERENCES [dbo].[LookupCategory] ([LookupCategoryID])
GO
ALTER TABLE [dbo].[LookupValue] CHECK CONSTRAINT [FK_LookupValue_LookupCategory_LookupCategoryID]
GO
ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_Demographic_DemographicID] FOREIGN KEY([DemographicID])
REFERENCES [dbo].[Demographic] ([DemographicID])
GO
ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_Demographic_DemographicID]
GO
ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_BabyFoodStageID] FOREIGN KEY([BabyFoodStageID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_BabyFoodStageID]
GO
ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_EmploymentStatusID] FOREIGN KEY([EmploymentStatusID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_EmploymentStatusID]
GO
ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_GenderID] FOREIGN KEY([GenderID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_GenderID]
GO
ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_RaceID] FOREIGN KEY([RaceID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_RaceID]
GO
ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_TransportationID] FOREIGN KEY([TransportationID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_TransportationID]
GO
ALTER TABLE [dbo].[MannaPickup]  WITH CHECK ADD  CONSTRAINT [FK_MannaPickup_LookupValue] FOREIGN KEY([PickupLocationId])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO
ALTER TABLE [dbo].[MannaPickup] CHECK CONSTRAINT [FK_MannaPickup_LookupValue]
GO
ALTER TABLE [dbo].[MannaPickup]  WITH CHECK ADD  CONSTRAINT [FK_MannaPickup_MannaData] FOREIGN KEY([MannaDataId])
REFERENCES [dbo].[MannaData] ([MannaDataID])
GO
ALTER TABLE [dbo].[MannaPickup] CHECK CONSTRAINT [FK_MannaPickup_MannaData]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "LookupValue"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 226
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Ethnicity_Manna'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Ethnicity_Manna'
GO
