--select everything into a working table for ease of movement
SELECT mrd.ID AS BarcodeID
, mrd.Notes
, mrd.PickupDate
, CASE PickupLocation
	WHEN 'ECS' THEN 116
	WHEN 'LBC' THEN 113
	WHEN 'MAIN' THEN 110
	WHEN 'STC' THEN 111
	WHEN 'WSS' THEN 112
	ELSE NULL
END AS PickupLocationID
INTO #tmpMannaReferralConversion
FROM [PantryAccessImport].dbo.MannaReferralDate AS mrd
WHERE PickupLocation IS NOT NULL 

select * from #tmpMannaReferralConversion

--insert from the working table
INSERT INTO MannaPickup (MannaDataId, PickupDate, PickupLocationId, Notes)
SELECT MannaData.MannaDataID, 
PickupDate, PickupLocationID, Notes 
FROM #tmpMannaReferralConversion
INNER JOIN Demographic ON #tmpMannaReferralConversion.BarcodeID = Demographic.BarcodeId
INNER JOIN MannaData ON MannaData.DemographicID = Demographic.DemographicID 
WHERE PickupLocationID IS NOT NULL
AND PickupDate IS NOT NULL

--select * from [SCAccessCopies].dbo.MannaReferralDate
--select PickupLocation, count(*) from [SCAccessCopies].dbo.MannaReferralDate group by PickupLocation
--select * from LookupValue where LookupCategoryID = 15
