﻿// Write your Javascript code.
$(function () {
    //use this class to give the user focus on the first field of inerest when the page loads
    $(".FocusOnLoad").focus().select();
    //enable bootstrap and jquery tooltip on any element
    $("body").tooltip({ selector: '[data-toggle=tooltip]' });

    var stCamillusDataEntryFormChangeHandler = function(event){ 
        var msg = "Are you sure you want to leave this page before saving your changes?";
        event.returnValue = msg;
        return msg;
    };

    $("form.dataEntry :input").change(function(){
        if (!$(this).closest('form').data('changed')) {
            window.addEventListener('beforeunload', stCamillusDataEntryFormChangeHandler);
        };
    });

    $('form.dataEntry').submit(function()   {
        window.removeEventListener('beforeunload', stCamillusDataEntryFormChangeHandler);
    });

    var loader = $('#loader');
    var loaderForm = $("form.loader");

    loaderForm.bind("invalid-form", function () {
        loader.hide();
    });

    //any form with the loader class can now trigger the loader animation on submit
    $('form.loader').submit(function () {
        loader.show();
    });


    //any link with the loader class can now trigger the loader animation on click
    $('a.loader').click(function () {
        loader.show();
        loader.delay(8000).hide('slow');
    });
});
// disable mousewheel on an input number field when in focus
// (to prevent Cromium browsers change the value when scrolling)
$("form").on("focus", "input[type=number]", function (e) {
    $(this).on("mousewheel.disableScroll", function (e) {
        e.preventDefault();
    });
});
//re-enable on blur to allow mousewheel scroll of window
$("form").on("blur", "input[type=number]", function (e) {
    $(this).off("mousewheel.disableScroll");
});
//auto-hide success message
$("#successMsg.autoFade").delay(6000);
