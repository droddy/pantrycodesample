﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.ComponentModel.DataAnnotations;

namespace StCamillus.Models
{
    public class DemographicAddEditViewModel
    {
        public Demographic Demographic { get; set; }

        public int[] SelectedNeedIds { get; set; }

        [Display(Name = "Needs: (ctrl+click for multiple)")]
        public MultiSelectList Needs { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Most Recent Distribution:")]
        public DateTime? MostRecentDistributionDate { get; set; }

        public string RecordDistroClass
        {
            get
            {
                return ShouldRecordDistro()
                    ? "btn-dark"
                    : "btn-danger";
            }
        }
        public string RecordDistroToolTip
        {
            get
            {
                return ShouldRecordDistro()
                    ? "Click to record a new distribution."
                    : "Please do not distribute unless this is a very special case.";
            }
        }
        private bool ShouldRecordDistro() { return MostRecentDistributionDate == null || MostRecentDistributionDate.Value.Date.AddDays(26) <= DateTime.Now.Date; }
    }
}
