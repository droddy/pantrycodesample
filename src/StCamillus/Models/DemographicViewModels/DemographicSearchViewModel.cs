﻿using System.ComponentModel.DataAnnotations;
namespace StCamillus.Models
{
    public class DemographicSearchViewModel
    {
        [Display(Name = "Last Name")]

        public string LastName { get; set; }
        [Display(Name = "First Name")]

        public string FirstName { get; set; }
        [Display(Name = "Address")]

        public string StreetAddress { get; set; }
        [Display(Name = "City")]

        public string City { get; set; }
        [Display(Name = "State")]

        public int? StateId { get; set; }
        [Display(Name = "County")]

        public int? CountyId { get; set; }
        [Display(Name = "Zip Code")]

        public string ZipCode { get; set; }
        [Display(Name = "Phone Number")]

        public string PhoneNumber { get; set; }
        [Display(Name = "Card ID")]

        public int BarcodeId { get; set; }

        public virtual LookupValue County { get; set; }

        public virtual LookupValue State { get; set; }
    }
}
