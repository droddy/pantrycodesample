﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace StCamillus.Models
{
    public partial class StCamillusContext : DbContext
    {
        public StCamillusContext(DbContextOptions<StCamillusContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Demographic>(entity =>
            {
                entity.HasIndex(e => e.BarcodeId)
                    .HasName("IX_Demographic_BarcodeId")
                    .IsUnique();

                entity.Property(e => e.DemographicId).HasColumnName("DemographicID");

                entity.Property(e => e.Age)
                    .HasColumnType("numeric")
                    .HasComputedColumnSql( "cast(floor(datediff(day,[DOB],getdate())/(365.25)) AS int)" )
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.BiWeeklyIncomeId).HasColumnName("BiWeeklyIncomeID");

                entity.Property(e => e.ChildrenFifteenToEighteen).HasDefaultValueSql("0");

                entity.Property(e => e.ChildrenSixToFourteen).HasDefaultValueSql("0");

                entity.Property(e => e.ChildrenThreeToFive).HasDefaultValueSql("0");

                entity.Property(e => e.ChildrenZeroToTwo).HasDefaultValueSql("0");

                entity.Property(e => e.City).HasMaxLength(100);

                entity.Property(e => e.CountyId).HasColumnName("CountyID");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("date");

                entity.Property(e => e.EthnicityId).HasColumnName("EthnicityID");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.MarriageStatusId).HasColumnName("MarriageStatusID");

                entity.Property(e => e.OriginCountryId).HasColumnName("OriginCountryID");

                entity.Property(e => e.PhoneNumber).HasMaxLength(20);

                entity.Property(e => e.StateId).HasColumnName("StateID");

                entity.Property(e => e.StreetAddress).HasMaxLength(255);

                entity.Property(e => e.ZipCode).HasMaxLength(15);

                entity.HasOne(d => d.BiWeeklyIncome)
                    .WithMany(p => p.DemographicBiWeeklyIncome)
                    .HasForeignKey(d => d.BiWeeklyIncomeId);

                entity.HasOne(d => d.County)
                    .WithMany(p => p.DemographicCounty)
                    .HasForeignKey(d => d.CountyId);

                entity.HasOne(d => d.Ethnicity)
                    .WithMany(p => p.DemographicEthnicity)
                    .HasForeignKey(d => d.EthnicityId);

                entity.HasOne(d => d.MarriageStatus)
                    .WithMany(p => p.DemographicMarriageStatus)
                    .HasForeignKey(d => d.MarriageStatusId);

                entity.HasOne(d => d.OriginCountry)
                    .WithMany(p => p.DemographicOriginCountry)
                    .HasForeignKey(d => d.OriginCountryId);

                entity.HasOne(d => d.State)
                    .WithMany(p => p.DemographicState)
                    .HasForeignKey(d => d.StateId);

                entity.HasMany(d => d.DemographicMultiValue).WithOne(p => p.Demographic);
            });

            modelBuilder.Entity<DemographicMultiValue>(entity =>
            {
                entity.Property(e => e.DemographicMultiValueId).HasColumnName("DemographicMultiValueID");

                entity.Property(e => e.DemographicId).HasColumnName("DemographicID");

                entity.Property(e => e.LookupValueId).HasColumnName("LookupValueID");

                entity.HasOne(d => d.Demographic)
                    .WithMany(p => p.DemographicMultiValue)
                    .HasForeignKey(d => d.DemographicId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.LookupValue)
                    .WithMany(p => p.DemographicMultiValue)
                    .HasForeignKey(d => d.LookupValueId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Distribution>(entity =>
            {
                entity.Property(e => e.DistributionId).HasColumnName("DistributionID");

                entity.Property(e => e.DemographicId).HasColumnName("DemographicID");

                entity.Property(e => e.DistributionDate).HasColumnType("date");

                entity.Property(e => e.PantryLocationId).HasColumnName("PantryLocationID");

                entity.HasOne(d => d.Demographic)
                    .WithMany(p => p.Distributions)
                    .HasForeignKey(d => d.DemographicId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.PantryLocation)
                    .WithMany(p => p.Distribution)
                    .HasForeignKey(d => d.PantryLocationId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<LookupCategory>(entity =>
            {
                entity.Property(e => e.LookupCategoryId).HasColumnName("LookupCategoryID");

                entity.Property(e => e.LookupCategoryName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<LookupValue>(entity =>
            {
                entity.Property(e => e.LookupValueId).HasColumnName("LookupValueID");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.LookupCategoryId).HasColumnName("LookupCategoryID");

                entity.Property(e => e.Visible).HasDefaultValueSql("1");

                entity.HasOne(d => d.LookupCategory)
                    .WithMany(p => p.LookupValue)
                    .HasForeignKey(d => d.LookupCategoryId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<MannaData>(entity =>
            {
                entity.HasIndex(e => e.DemographicId)
                    .HasName("IX_MannaData_DemographicID")
                    .IsUnique();

                entity.Property(e => e.MannaDataId).HasColumnName("MannaDataID");

                entity.Property(e => e.BabyFoodStageId).HasColumnName("BabyFoodStageID");

                entity.Property(e => e.DemographicId).HasColumnName("DemographicID");

                entity.Property(e => e.EmailAddress).HasMaxLength(100);

                entity.Property(e => e.EmergencyContactName).HasMaxLength(100);

                entity.Property(e => e.EmergencyContactPhone).HasMaxLength(100);

                entity.Property(e => e.EmploymentStatusId).HasColumnName("EmploymentStatusID");

                entity.Property(e => e.FoodStampDollars).HasColumnType("money");

                entity.Property(e => e.FormulaType).HasMaxLength(100);

                entity.Property(e => e.GenderId).HasColumnName("GenderID");

                entity.Property(e => e.IsEmployed).HasDefaultValueSql("0");

                entity.Property(e => e.IsReceivingEnergyAssistance).HasDefaultValueSql("0");

                entity.Property(e => e.IsUsingMedicaid).HasDefaultValueSql("0");

                entity.Property(e => e.MonthlyGrossDollars).HasColumnType("money");

                entity.Property(e => e.NeedsDiabeticBox).HasDefaultValueSql("0");

                entity.Property(e => e.NeedsVegetarianBox).HasDefaultValueSql("0");

                entity.Property(e => e.OtherDollars).HasColumnType("money");

                entity.Property(e => e.PrimaryLanguage).HasMaxLength(50);

                entity.Property(e => e.RaceId).HasColumnName("RaceID");

                entity.Property(e => e.SocialSecurityDisabilityInsuranceDollars).HasColumnType("money");

                entity.Property(e => e.SocialSecurityInsuranceDollars).HasColumnType("money");

                entity.Property(e => e.Tanfdollars)
                    .HasColumnName("TANFDollars")
                    .HasColumnType("money");

                entity.Property(e => e.TransportationId).HasColumnName("TransportationID");

                entity.Property(e => e.UnemploymentDollars).HasColumnType("money");

                entity.Property(e => e.YearlyGrossDollars).HasColumnType("money");

                entity.HasOne(d => d.BabyFoodStage)
                    .WithMany(p => p.MannaDataBabyFoodStage)
                    .HasForeignKey(d => d.BabyFoodStageId);

                entity.HasOne(d => d.Demographic)
                    .WithOne(p => p.MannaData)
                    .HasForeignKey<MannaData>(d => d.DemographicId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.EmploymentStatus)
                    .WithMany(p => p.MannaDataEmploymentStatus)
                    .HasForeignKey(d => d.EmploymentStatusId);

                entity.HasOne(d => d.Gender)
                    .WithMany(p => p.MannaDataGender)
                    .HasForeignKey(d => d.GenderId);

                entity.HasOne(d => d.Race)
                    .WithMany(p => p.MannaDataRace)
                    .HasForeignKey(d => d.RaceId);

                entity.HasOne(d => d.Transportation)
                    .WithMany(p => p.MannaDataTransportation)
                    .HasForeignKey(d => d.TransportationId);

                entity.HasMany(m => m.MannaPickups).WithOne(m => m.MannaData);
            });

            modelBuilder.Entity<MannaPickup>(entity =>
            {
                entity.Property(e => e.MannaPickupId).HasColumnName("MannaPickupId");

                entity.Property(e => e.MannaDataId).HasColumnName("MannaDataId");

                entity.Property(e => e.PickupLocationId).HasColumnName("PickupLocationId");

                entity.Property(e => e.Notes).HasColumnName("Notes");

                entity.Property(e => e.PickupDate).HasColumnType("date");

                entity.HasOne(m => m.MannaData)
                    .WithMany(m => m.MannaPickups)
                    .HasForeignKey(m => m.MannaDataId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.PickupLocation)
                    .WithMany(p => p.MannaPickup)
                    .HasForeignKey(d => d.PickupLocationId)
                    .OnDelete(DeleteBehavior.Restrict);

            });
        }

        public virtual DbSet<Demographic> Demographic { get; set; }

        public virtual DbSet<DemographicMultiValue> DemographicMultiValue { get; set; }

        public virtual DbSet<Distribution> Distribution { get; set; }

        public virtual DbSet<LookupCategory> LookupCategory { get; set; }

        public virtual DbSet<LookupValue> LookupValue { get; set; }

        public virtual DbSet<MannaData> MannaData { get; set; }

        public virtual DbSet<MannaPickup> MannaPickup { get; set; }
    }
}