﻿using System;
using System.Collections.Generic;

namespace StCamillus.Models
{
    public partial class LookupValue
    {
        public LookupValue()
        {
            DemographicBiWeeklyIncome = new HashSet<Demographic>();
            DemographicCounty = new HashSet<Demographic>();
            DemographicEthnicity = new HashSet<Demographic>();
            DemographicMarriageStatus = new HashSet<Demographic>();
            DemographicOriginCountry = new HashSet<Demographic>();
            DemographicState = new HashSet<Demographic>();
            DemographicMultiValue = new HashSet<DemographicMultiValue>();
            Distribution = new HashSet<Distribution>();
            MannaDataBabyFoodStage = new HashSet<MannaData>();
            MannaDataEmploymentStatus = new HashSet<MannaData>();
            MannaDataGender = new HashSet<MannaData>();
            MannaDataRace = new HashSet<MannaData>();
            MannaDataTransportation = new HashSet<MannaData>();
            MannaPickup = new HashSet<MannaPickup>();
        }

        public int LookupValueId { get; set; }
        public int LookupCategoryId { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
        public short? SortOrder { get; set; }

        public virtual ICollection<Demographic> DemographicBiWeeklyIncome { get; set; }
        public virtual ICollection<Demographic> DemographicCounty { get; set; }
        public virtual ICollection<Demographic> DemographicEthnicity { get; set; }
        public virtual ICollection<Demographic> DemographicMarriageStatus { get; set; }
        public virtual ICollection<Demographic> DemographicOriginCountry { get; set; }
        public virtual ICollection<Demographic> DemographicState { get; set; }
        public virtual ICollection<DemographicMultiValue> DemographicMultiValue { get; set; }
        public virtual ICollection<Distribution> Distribution { get; set; }
        public virtual ICollection<MannaData> MannaDataBabyFoodStage { get; set; }
        public virtual ICollection<MannaData> MannaDataEmploymentStatus { get; set; }
        public virtual ICollection<MannaData> MannaDataGender { get; set; }
        public virtual ICollection<MannaData> MannaDataRace { get; set; }
        public virtual ICollection<MannaData> MannaDataTransportation { get; set; }
        public virtual ICollection<MannaPickup> MannaPickup { get; set; }
        public virtual LookupCategory LookupCategory { get; set; }
    }
}
