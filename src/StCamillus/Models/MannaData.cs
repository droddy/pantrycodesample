﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StCamillus.Models
{
    public partial class MannaData
    {
        public MannaData()
        {
            MannaPickups = new HashSet<MannaPickup>();
        }

        public int MannaDataId { get; set; }

        public int DemographicId { get; set; }

        [Display(Name = "Gender")]
        public int? GenderId { get; set; }

        [Display(Name = "If yes, choose one:")]
        public int? EmploymentStatusId { get; set; }

        [Display(Name = "Race")]
        public int? RaceId { get; set; }

        [Display(Name = "How do you get to us?")]
        public int? TransportationId { get; set; }

        [Display(Name = "Baby Food Stage")]
        public int? BabyFoodStageId { get; set; }

        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Display(Name = "No. of Children")]
        public short? NumberOfChildren { get; set; }

        [Display(Name = "Diabetic Box")]
        public bool NeedsDiabeticBox { get; set; }

        [Display(Name = "Vegetarian Box")]
        public bool NeedsVegetarianBox { get; set; }

        [Display(Name = "Formula Type")]
        public string FormulaType { get; set; }

        [Display(Name = "Is the client currently employed?")]
        public bool IsEmployed { get; set; }

        [Display(Name = "* Monthly Gross $")]
        public decimal? MonthlyGrossDollars { get; set; }

        [Display(Name = "* Yearly Gross $")]
        public decimal? YearlyGrossDollars { get; set; }

        [Display(Name = "Food Stamp $")]
        public decimal? FoodStampDollars { get; set; }

        [Display(Name = "Unemployment $")]
        public decimal? UnemploymentDollars { get; set; }

        [Display(Name = "Medicaid")]
        public bool IsUsingMedicaid { get; set; }

        [Display(Name = "SSI $")]
        public decimal? SocialSecurityInsuranceDollars { get; set; }

        [Display(Name = "SSDI $")]
        public decimal? SocialSecurityDisabilityInsuranceDollars { get; set; }

        [Display(Name = "Energy Assistance")]
        public bool IsReceivingEnergyAssistance { get; set; }

        [Display(Name = "TANF $")]
        public decimal? Tanfdollars { get; set; }

        [Display(Name = "Other $")]
        public decimal? OtherDollars { get; set; }

        [Display(Name = "Primary Language")]
        public string PrimaryLanguage { get; set; }

        [Display(Name = "Emergency Contact Name")]
        public string EmergencyContactName { get; set; }

        [Display(Name = "Emergency Contact Phone")]
        public string EmergencyContactPhone { get; set; }

        public virtual LookupValue BabyFoodStage { get; set; }

        public virtual Demographic Demographic { get; set; }

        public virtual LookupValue EmploymentStatus { get; set; }

        public virtual LookupValue Gender { get; set; }

        public virtual LookupValue Race { get; set; }

        public virtual LookupValue Transportation { get; set; }

        public virtual ICollection<MannaPickup> MannaPickups { get; set; } 
    }
}


