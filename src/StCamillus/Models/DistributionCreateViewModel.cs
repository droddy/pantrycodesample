﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StCamillus.Models
{
    public class DistributionCreateViewModel
    {
        [Display( Name = "Demographic ID" )]
        public int DemographicId { get; set; }

        //there is a problem with client-side date validation
        //where, if you set the model DataType attribute to Date, this enables HTML 5 client-side validation
        //but if you want to use something like jQuery UI's datepicker (more compatability) the Date DataType will work
        //but then you have both the HTML5 picker combined with the jQuery UI picker and there is also the chance that the HTML 5 date format won't agree 
        //with the format jQuery UI is setup for
        //[DisplayFormat( DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true )]
        //Trying HTML 5 picker again
        [DataType( DataType.Date )]
        [Display( Name = "Date" )]
        public DateTime? DistributionDate { get; set; }

        [Display( Name = "Infant Formula?" )]
        public bool GaveInfantFormula { get; set; }

        [Display( Name = "Pantry" )]
        public int PantryLocationId { get; set; }

        public LookupValue PantryLocation { get; set; }

        public Demographic Demographic { get; set; }

        //there is a problem with client-side date validation
        //where, if you set the model DataType attribute to Date, this enables HTML 5 client-side validation
        //but if you want to use something like jQuery UI's datepicker (more compatability) the Date DataType will work
        //but then you have both the HTML5 picker combined with the jQuery UI picker and there is also the chance that the HTML 5 date format won't agree 
        //with the format jQuery UI is setup for
        //[DisplayFormat( DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true )]
        //Trying HTML 5 picker again
        [DataType( DataType.Date )]
        [Display( Name = "Most Recent Distribution" )]
        public DateTime? MostRecentDistributionDate { get; set; }
    }
}
