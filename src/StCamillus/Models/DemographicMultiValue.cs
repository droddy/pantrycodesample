﻿namespace StCamillus.Models
{
    public partial class DemographicMultiValue
    {
        public int DemographicMultiValueId { get; set; }

        public int DemographicId { get; set; }

        public int LookupValueId { get; set; }

        public virtual Demographic Demographic { get; set; }

        public virtual LookupValue LookupValue { get; set; }
    }
}
