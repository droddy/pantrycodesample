﻿using System.Collections.Generic;

namespace StCamillus.Models
{
    public class DistributionDemographicIndexViewModel
    {
        public Demographic Demographic { get; set; }

        public IEnumerable<Distribution> Distributions { get; set; }
    }
}
