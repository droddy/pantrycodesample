﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StCamillus.Models
{
    public partial class Demographic
    {
        public Demographic()
        {
            DemographicMultiValue = new HashSet<DemographicMultiValue>();
            Distributions = new HashSet<Distribution>();
        }

        [Display(Name = "Demographic Id")]
        public int DemographicId { get; set; }

        [Display(Name = "Last Name"), Required]
        public string LastName { get; set; }

        [Display(Name = "First Name"), Required]
        public string FirstName { get; set; }

        [Display(Name = "Address")]
        public string StreetAddress { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "State")]
        public int? StateId { get; set; }

        [Display(Name = "County")]
        public int? CountyId { get; set; }

        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Country of Origin")]
        public int? OriginCountryId { get; set; }

        [Display(Name = "Ethnicity")]
        public int? EthnicityId { get; set; }

        [Display(Name = "Marriage Status")]
        public int? MarriageStatusId { get; set; }

        [Display(Name = "Number of People in Family")]
        public short? TotalFamilyMembers { get; set; }

        public short? Adults { get; set; }

        [Display(Name = "0-2 years")]
        public bool ChildrenZeroToTwo { get; set; }

        [Display(Name = "3-5 years")]
        public bool ChildrenThreeToFive { get; set; }

        [Display(Name = "6-14 years")]
        public bool ChildrenSixToFourteen { get; set; }

        [Display(Name = "15-18 years")]
        public bool ChildrenFifteenToEighteen { get; set; }

        public string Note { get; set; }

        [Display(Name = "2018 MD Self Disclosure Form")]
        public bool SelfDisclosure { get; set; }

        public short? Children { get; set; }

        //there has been a problem with client-side date validation
        //where, if you set the model DataType attribute to Date, this enables HTML 5 client-side validation
        //but if you want to use something like jQuery UI's datepicker (more compatability) the Date DataType will work
        //but then you have both the HTML5 picker combined with the jQuery UI picker and there is also the chance that the HTML 5 date format won't agree 
        //with the format jQuery UI is setup for
        //jQuery UI Date Picker
        //[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        //let's try html 5 again
        [DataType(DataType.Date)]
        [Display(Name = "* Date of Birth")]
        public DateTime? Dob { get; set; }

        [DisplayFormat(DataFormatString = "{0:D}")]
        [Display(Name = "Age")]
        public int? FlattenedAge { get { return Convert.ToInt32( Age ?? Age ); } }

        public decimal? Age { get; set; }

        [Display(Name = "Bi-weekly Income")]
        public int? BiWeeklyIncomeId { get; set; }

        [Display(Name = "Card ID"), Required]
        public int? BarcodeId { get; set; }

        public virtual ICollection<DemographicMultiValue> DemographicMultiValue { get; set; }

        public virtual ICollection<Distribution> Distributions { get; set; }

        public virtual MannaData MannaData { get; set; }

        public virtual LookupValue BiWeeklyIncome { get; set; }

        public virtual LookupValue County { get; set; }

        public virtual LookupValue Ethnicity { get; set; }

        public virtual LookupValue MarriageStatus { get; set; }

        public virtual LookupValue OriginCountry { get; set; }

        public virtual LookupValue State { get; set; }
    }
}
