﻿using Microsoft.AspNetCore.Identity;
using StCamillus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StCamillus.Models
{
    public class ApplicationUsersIndexViewModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private bool? _isUserAdmin;
        private bool? _isSilverSpring;
        private bool? _isLangleyPark;
        private const string ClickTo = "Click to ";
        private const string Disable = "Disable";
        private const string Enable = "Enable";
        private const string Yes = "Yes: ";
        private const string No = "No: ";


        public ApplicationUsersIndexViewModel(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public ApplicationUser ApplicationUser { get; set; }

        public string UserAdminToggleToolTip { get { return ClickTo + UserAdminToggleVerb; } }
        public string UserAdminToggleVerb { get { return IsUserAdmin ? Disable : Enable; } }
        public string UserAdminToggleBool { get { return IsUserAdmin ? Yes : No; } }
        public bool IsUserAdmin
        {
            get
            {
                if (_isUserAdmin == null)
                {
                    _isUserAdmin = _userManager.IsInRoleAsync(this.ApplicationUser, "User Admin").Result;
                }
                return _isUserAdmin.Value;
            }
        }

        public string SilverSpringToggleToolTip { get { return ClickTo + SilverSpringToggleVerb; } }
        public string SilverSpringToggleVerb { get { return IsSilverSpring ? Disable : Enable; } }
        public string SilverSpringToggleBool { get { return IsSilverSpring ? Yes : No; } }
        public bool IsSilverSpring
        {
            get
            {
                if (_isSilverSpring == null)
                {
                    _isSilverSpring = _userManager.IsInRoleAsync(this.ApplicationUser, "Silver Spring").Result;
                }
                return _isSilverSpring.Value;
            }
        }

        public string LangleyParkToggleToolTip { get { return ClickTo + LangleyParkToggleVerb; } }
        public string LangleyParkToggleVerb { get { return IsLangleyPark ? Disable : Enable; } }
        public string LangleyParkToggleBool { get { return IsLangleyPark ? Yes : No; } }
        public bool IsLangleyPark
        {
            get
            {
                if (_isLangleyPark == null)
                {
                    _isLangleyPark = _userManager.IsInRoleAsync(this.ApplicationUser, "Langley Park").Result;
                }
                return _isLangleyPark.Value;
            }
        }
    }
}
