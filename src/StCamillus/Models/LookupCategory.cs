﻿using System;
using System.Collections.Generic;

namespace StCamillus.Models
{
    public partial class LookupCategory
    {
        public LookupCategory()
        {
            LookupValue = new HashSet<LookupValue>();
        }

        public int LookupCategoryId { get; set; }
        public string LookupCategoryName { get; set; }

        public virtual ICollection<LookupValue> LookupValue { get; set; }
    }
}
