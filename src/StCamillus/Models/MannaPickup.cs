﻿using System;
using System.ComponentModel.DataAnnotations;

namespace StCamillus.Models
{
    public partial class MannaPickup
    {
        [Display(Name = "ID")]
        public int MannaPickupId { get; set; }

        public int MannaDataId { get; set; }

        //there has been a problem with client-side date validation
        //where, if you set the model DataType attribute to Date, this enables HTML 5 client-side validation
        //but if you want to use something like jQuery UI's datepicker (more compatability) the Date DataType will work
        //but then you have both the HTML5 picker combined with the jQuery UI picker and there is also the chance that the HTML 5 date format won't agree 
        //with the format jQuery UI is setup for
        //jQuery UI Date Picker
        //[DisplayFormat(DataFormatString = "{0:MM-dd-yyyy}", ApplyFormatInEditMode = true)]
        //let's try html 5 again
        [DataType( DataType.Date )]
        [Display(Name = "Pick-up Date")]
        public DateTime? PickupDate { get; set; }

        [Display(Name = "Pick-up Location")]
        public int PickupLocationId { get; set; }

        public string Notes { get; set; }

        public virtual MannaData MannaData { get; set; }

        public virtual LookupValue PickupLocation { get; set; }
    }
}
