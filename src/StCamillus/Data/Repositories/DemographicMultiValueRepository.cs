﻿using Microsoft.EntityFrameworkCore;
using StCamillus.Models;
using System.Collections.Generic;
using System.Linq;

namespace StCamillus.Data.Repositories
{
    public static class DemographicMultiValueRepository
    {
        public static bool DemographicMultiValueExists(StCamillusContext context, int demographicId, int lookupValueId)
        {
            return context.DemographicMultiValue.Any(dmv => dmv.DemographicId.Equals(demographicId) && dmv.LookupValueId.Equals(lookupValueId));
        }

        public static void HydrateDemographicMultiValues(StCamillusContext context, Demographic demographic)
        {
            var dmv = GetDemographicMultiValues(context, demographic.DemographicId);
            demographic.DemographicMultiValue = new HashSet<DemographicMultiValue> (dmv.ToList());
        }

        public static IQueryable<DemographicMultiValue> GetDemographicMultiValues(StCamillusContext context, int demographicId)
        {
            return context.DemographicMultiValue.Where(d => d.DemographicId.Equals(demographicId)).Include(d => d.LookupValue);
        }
    }
}
