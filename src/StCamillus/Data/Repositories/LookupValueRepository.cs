﻿using StCamillus.Models;
using System.Linq;

namespace StCamillus.Data.Repositories
{
    public static class LookupValueRepository
    {
        public static IQueryable<LookupValue> GetNeedsLookupValues(StCamillusContext context)
        {
            return context.LookupValue.Where(l => l.LookupCategoryId.Equals(7));
        }
    }
}
