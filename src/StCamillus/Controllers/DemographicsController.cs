using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StCamillus.Models;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using StCamillus.Data.Repositories;
using System.Collections.Generic;

namespace StCamillus.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "User Admin, Silver Spring, Langley Park")]
    public class DemographicsController : Controller
    {
        private readonly StCamillusContext _context;
        private readonly ITempDataDictionaryFactory _tempDataFactory;

        public DemographicsController(StCamillusContext context, ITempDataDictionaryFactory tempDataFactory)
        {
            _context = context;
            _tempDataFactory = tempDataFactory;
        }

        //// GET: Demographics
        //public async Task<IActionResult> Index()
        //{
        //    var demographics = _context.Demographic;
        //    return View(await demographics.ToListAsync());
        //}

        // GET: Demographics/Create
        public IActionResult Create()
        {
            ViewBag.ChangesMade = false;
            AddSelectLists();
            var viewModel = new DemographicAddEditViewModel { Demographic = new Demographic()};
            AddNeedsMultiSelect(viewModel);
            return View(viewModel);
        }

        // POST: Demographics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Demographic,Demographic.Adults,Demographic.Age,Demographic.BarcodeId,Demographic.BiWeeklyIncomeId,Demographic.Children,Demographic.ChildrenFifteenToEighteen,Demographic.ChildrenSixToFourteen,Demographic.ChildrenThreeToFive,Demographic.ChildrenZeroToTwo,Demographic.City,Demographic.CountyId,Demographic.Dob,Demographic.EthnicityId,Demographic.FirstName,Demographic.LastName,Demographic.MarriageStatusId,Demographic.Note,Demographic.OriginCountryId,Demographic.PhoneNumber,Demographic.SelfDisclosure,Demographic.StateId,Demographic.StreetAddress,Demographic.TotalFamilyMembers,Demographic.ZipCode,SelectedNeedIds,Needs")] DemographicAddEditViewModel viewModel)
        {
            if (_context.Demographic.Any(d => d.BarcodeId == viewModel.Demographic.BarcodeId))
            {
                ModelState.AddModelError("Card ID", "Card ID is already in use. Please search for the Card ID and modify the existing Demographic.");
            }

            if (ModelState.IsValid)
            {
                _context.Add(viewModel.Demographic);
                await _context.SaveChangesAsync();

                var demographic = await _context.Demographic.SingleOrDefaultAsync(m => m.BarcodeId == viewModel.Demographic.BarcodeId);
                if (demographic == null)
                {
                    return NotFound();
                }

                var demographicMultiValues = DemographicMultiValueRepository.GetDemographicMultiValues(_context, demographic.DemographicId);
                foreach (var need in LookupValueRepository.GetNeedsLookupValues(_context))
                {
                    var needId = need.LookupValueId;

                    if (viewModel.SelectedNeedIds != null && viewModel.SelectedNeedIds.Contains(needId) && !DemographicMultiValueRepository.DemographicMultiValueExists(_context, demographic.DemographicId, needId))
                    {
                        _context.Add(new DemographicMultiValue { DemographicId = demographic.DemographicId, LookupValueId = needId });
                    }
                }
                await _context.SaveChangesAsync();

                return RedirectToAction("Edit", new { id = demographic.DemographicId });
            }

            ViewBag.ChangesMade = false;
            AddSelectLists();
            AddNeedsMultiSelect(viewModel);
            return View(viewModel);
        }

        // GET: Demographics/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if ( id == null )
            {
                return NotFound();
            }

            var demographic = await _context.Demographic.Include( d => d.Distributions ).SingleOrDefaultAsync( m => m.DemographicId == id );
            demographic.DemographicMultiValue = new HashSet<DemographicMultiValue>( DemographicMultiValueRepository.GetDemographicMultiValues( _context, demographic.DemographicId ) );

            if ( demographic == null )
            {
                return RedirectToAction( "Find", new { id, notFound = true } );
            }

            Distribution lastDistro = await GetLastDistro( demographic.DemographicId );

            ViewBag.ChangesMade = false;
            AddSelectLists();

            var viewModel = new DemographicAddEditViewModel { Demographic = demographic, MostRecentDistributionDate = lastDistro?.DistributionDate };
            AddNeedsMultiSelect( viewModel );
            return View( viewModel );
        }

        // POST: Demographics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Demographic,Demographic.DemographicId,Demographic.Adults,Demographic.Age,Demographic.BarcodeId,Demographic.BiWeeklyIncomeId,Demographic.Children,Demographic.ChildrenFifteenToEighteen,Demographic.ChildrenSixToFourteen,Demographic.ChildrenThreeToFive,Demographic.ChildrenZeroToTwo,Demographic.City,Demographic.CountyId,Demographic.Dob,Demographic.EthnicityId,Demographic.FirstName,Demographic.LastName,Demographic.MarriageStatusId,Demographic.Note,Demographic.OriginCountryId,Demographic.PhoneNumber,Demographic.SelfDisclosure,Demographic.StateId,Demographic.StreetAddress,Demographic.TotalFamilyMembers,Demographic.ZipCode,SelectedNeedIds,Needs")] DemographicAddEditViewModel viewModel)
        {
            ViewBag.ChangesMade = false;
            if (id != viewModel.Demographic.DemographicId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                var demographicMultiValues = DemographicMultiValueRepository.GetDemographicMultiValues(_context, viewModel.Demographic.DemographicId);//_context.DemographicMultiValue.Where(dmv => dmv.DemographicId.Equals(viewModel.Demographic.DemographicId));

                foreach (var need in LookupValueRepository.GetNeedsLookupValues(_context))
                {
                    var needId = need.LookupValueId;

                    if ((viewModel.SelectedNeedIds == null || !viewModel.SelectedNeedIds.Contains(needId)) && DemographicMultiValueRepository.DemographicMultiValueExists(_context, viewModel.Demographic.DemographicId, needId))
                    {
                        _context.Remove(demographicMultiValues.Single(dmv => dmv.DemographicId.Equals(viewModel.Demographic.DemographicId) && dmv.LookupValueId.Equals(needId)));
                    }

                    if (viewModel.SelectedNeedIds != null && viewModel.SelectedNeedIds.Contains(needId) && !DemographicMultiValueRepository.DemographicMultiValueExists(_context, viewModel.Demographic.DemographicId, needId))
                    {
                        _context.Add(new DemographicMultiValue { DemographicId = viewModel.Demographic.DemographicId, LookupValueId = needId });
                    }
                }

                try
                {

                    _context.Update(viewModel.Demographic);
                    await _context.SaveChangesAsync();
                    ViewBag.ChangesMade = true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DemographicExists(viewModel.Demographic.DemographicId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            AddSelectLists();
            Distribution lastDistro = await GetLastDistro( viewModel.Demographic.DemographicId );
            viewModel.MostRecentDistributionDate = lastDistro?.DistributionDate;
            AddNeedsMultiSelect( viewModel );
            return View(viewModel);
        }

        public ActionResult Find(int id = 0, bool notFound = false)
        {
            var model = new DemographicSearchViewModel();

            //this is where we send the user if she arrives at EditDemographic/5 when there is no Demographic with id = 5
            if (notFound)
            {
                model.BarcodeId = id;
                ModelState.AddModelError("", "Not found. Please try again.");
            }

            // If we got to this action because of a saved distribution, there should be a message if the save was successful.
            // Alert the user that the save worked.
            var tempData = _tempDataFactory.GetTempData(this.HttpContext);
            var successMessage = tempData["SuccessMessage"];

            if (null != successMessage)
            {
                ViewBag.SuccessMessage = successMessage.ToString();
            }
            AddSelectLists();
            ViewData["CountyId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(2)), "LookupValueId", "Description");
            ViewData["StateId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(1)), "LookupValueId", "Description");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Find([Bind("LastName,FirstName,StreetAddress,City,StateId,CountyId,ZipCode,PhoneNumber,BarcodeId")] DemographicSearchViewModel searchModel)
        {
            if (searchModel.BarcodeId == 0
                && string.IsNullOrWhiteSpace(searchModel.FirstName)
                && string.IsNullOrWhiteSpace(searchModel.LastName)
                && string.IsNullOrWhiteSpace(searchModel.StreetAddress)
                && string.IsNullOrWhiteSpace(searchModel.City)
                && string.IsNullOrWhiteSpace(searchModel.ZipCode)
                && string.IsNullOrWhiteSpace(searchModel.PhoneNumber)
                && searchModel.StateId == 0
                && searchModel.CountyId == 0)
            {
                var tempData = _tempDataFactory.GetTempData(this.HttpContext);
                tempData["SuccessMessage"] = "You must provide some search data.";

                return RedirectToAction("Find");
            }

            IQueryable<Demographic> search = _context.Demographic.OrderBy( d => d.LastName.Trim() ).ThenBy(d => d.FirstName.Trim());

            if (searchModel.BarcodeId != 0)
            {
                search = search.Where(d => d.BarcodeId == searchModel.BarcodeId);
            }

            if (!string.IsNullOrWhiteSpace(searchModel.FirstName))
            {
                search = search.Where( d => d.FirstName.Contains( searchModel.FirstName ) );
            }

            if (!string.IsNullOrWhiteSpace(searchModel.LastName))
            {
                search = search.Where(d => d.LastName.Contains(searchModel.LastName));
            }

            if (!string.IsNullOrWhiteSpace(searchModel.StreetAddress))
            {
                search = search.Where(d => d.StreetAddress.Contains(searchModel.StreetAddress));
            }

            if (!string.IsNullOrWhiteSpace(searchModel.City))
            {
                search = search.Where(d => d.City.Contains(searchModel.City));
            }

            if (searchModel.StateId != null && searchModel.StateId != 0)
            {
                search = search.Where(d => d.StateId == searchModel.StateId);
            }

            if (searchModel.CountyId != null && searchModel.CountyId != 0)
            {
                search = search.Where(d => d.CountyId == searchModel.CountyId);
            }

            if (!string.IsNullOrWhiteSpace(searchModel.ZipCode))
            {
                search = search.Where(d => d.ZipCode.Contains(searchModel.ZipCode));
            }

            if (!string.IsNullOrWhiteSpace(searchModel.PhoneNumber))
            {
                search = search.Where(d => d.PhoneNumber.Contains(searchModel.PhoneNumber));
            }

            //var demographics = search.Select(d => d);
            var list = search.ToList();

            if ( list.Count == 1 )
            {
                return RedirectToAction("Edit", new { id = list[ 0 ].DemographicId } );
            }

            return View("Index", list);
        }

        //// GET: Demographics/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var demographic = await _context.Demographic.SingleOrDefaultAsync(m => m.DemographicId == id);
        //    if (demographic == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(demographic);
        //}

        // POST: Demographics/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var demographic = await _context.Demographic.SingleOrDefaultAsync(m => m.DemographicId == id);
        //    _context.Demographic.Remove(demographic);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        private bool DemographicExists(int id)
        {
            return _context.Demographic.Any(e => e.DemographicId == id);
        }

        private void AddSelectLists()
        {
            ViewData["BiWeeklyIncomeId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(9)), "LookupValueId", "Description");
            ViewData["CountyId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(2)).OrderBy( v => v.Description ), "LookupValueId", "Description");
            ViewData["EthnicityId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(4)).OrderBy(v => v.Description), "LookupValueId", "Description");
            ViewData["MarriageStatusId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(5)).OrderBy( v => v.Description ), "LookupValueId", "Description");
            ViewData["OriginCountryId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(3)).OrderBy( v => v.Description ), "LookupValueId", "Description");
            ViewData["StateId"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(1)).OrderBy( v => v.Description ), "LookupValueId", "Description");
        }

        private void AddNeedsMultiSelect( DemographicAddEditViewModel viewModel )
        {
            const int needsCategoryId = 7;
            viewModel.Needs = new MultiSelectList( LookupValueRepository.GetNeedsLookupValues( _context ), "LookupValueId", "Description" );
            viewModel.SelectedNeedIds = viewModel.Demographic.DemographicMultiValue.Where( dmv => dmv.LookupValue.LookupCategoryId.Equals( needsCategoryId ) ).Select( dmv => dmv.LookupValueId ).ToArray();
        }

        private async Task<Distribution> GetLastDistro( int demographicId )
        {
            return await _context.Distribution.OrderByDescending( d => d.DistributionDate ).FirstOrDefaultAsync( d => d.DemographicId == demographicId );
        }
    }
}
