﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StCamillus.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace StCamillus.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "User Admin, Silver Spring, Langley Park")]
    public class DistributionController : Controller
    {
        private readonly StCamillusContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ITempDataDictionaryFactory _tempDataFactory;

        public DistributionController(StCamillusContext context, UserManager<ApplicationUser> userManager, ITempDataDictionaryFactory tempDataFactory)
        {
            _context = context;
            _userManager = userManager;
            _tempDataFactory = tempDataFactory;
        }

        public async Task<IActionResult> Index(int id)
        {
            var distributions = _context.Distribution.Where(x => x.DemographicId == id).Include(d => d.Demographic);
            var demographic = await _context.Demographic.SingleOrDefaultAsync(x => x.DemographicId == id);
            var viewModel = new DistributionDemographicIndexViewModel { Demographic = demographic, Distributions = distributions.ToList() };
            return View(viewModel);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Distribution distribution = await _context.Distribution.SingleOrDefaultAsync(d => d.DistributionId == id);

            if (distribution == null)
            {
                return NotFound();
            }

            return View(distribution);
        }

        // GET: Distributions/Create
        [HttpGet]
        public async Task<IActionResult> Create(int id)
        {
            var demographic = await _context.Demographic.SingleOrDefaultAsync(d => d.DemographicId == id);

            if (demographic == null)
            {
                return BadRequest();
            }

            var pantries = GetPantriesBasedOnRole();
            
            //this default only works based on the user session--the role information isn't updated
            //if the role is changed while the user is logged in. If an admin changes the roles
            //the user won't see this default change until logging off and back on again
            var pantry = await GetDefaultPantryBasedOnRoleAsync(pantries);

            if ( pantries.Count() > 1 )
            {
                AddSelectLists( pantries );

                Distribution lastDistro = await GetLastDistro( demographic.DemographicId );

                var model = new DistributionCreateViewModel
                {
                    Demographic = demographic,
                    DemographicId = id,
                    DistributionDate = DateTime.Now,
                    PantryLocationId = pantry.LookupValueId,
                    PantryLocation = pantry,
                    MostRecentDistributionDate = lastDistro?.DistributionDate
                };
                return View( model );
            }
            return await Create( new Distribution() { BagCount = 1, DemographicId = id, DistributionDate = DateTime.Now.Date, GaveInfantFormula = false, PantryLocationId = pantry.LookupValueId } );
        }
        private async Task<Distribution> GetLastDistro( int demographicId )
        {
            return await _context.Distribution.OrderByDescending( d => d.DistributionDate ).FirstOrDefaultAsync( d => d.DemographicId == demographicId );
        }

        private IQueryable<LookupValue> GetPantriesBasedOnRole()
        {
            var ssRole = User.IsInRole("Silver Spring");
            var lpRole = User.IsInRole("Langley Park");

            return (ssRole && lpRole) || (!ssRole && !lpRole)
                ? _context.LookupValue.Where(l => l.LookupCategoryId.Equals(8))
                : lpRole
                    ? _context.LookupValue.Where(l => l.LookupCategoryId.Equals(8) && l.Description == "Langley Park")
                    : _context.LookupValue.Where(l => l.LookupCategoryId.Equals(8) && l.Description == "Silver Spring");
        }

        private async Task<LookupValue> GetDefaultPantryBasedOnRoleAsync(IQueryable<LookupValue> pantries)
        {
            var ssRole = User.IsInRole("Silver Spring");
            var lpRole = User.IsInRole("Langley Park");
            return ssRole && lpRole
                ? new LookupValue()
                : lpRole
                    ? await pantries.SingleAsync(p => p.Description == "Langley Park")
                    : await pantries.SingleAsync(p => p.Description == "Silver Spring");
        }

        //private async void AlertOnRecentDistribution(int id)
        //{
        //    var lastDistribution = await _context.Distribution
        //        .OrderByDescending(d => d.DistributionDate)
        //        .FirstOrDefaultAsync(d => d.DemographicId.Equals(id));

        //    if (null != lastDistribution && lastDistribution.DistributionDate.HasValue && (DateTime.Now - lastDistribution.DistributionDate.Value).Days <= 30)
        //    {
        //        ViewBag.DistributionAlert = string.Format("ALERT: Recent Distribution: {0:d}", lastDistribution.DistributionDate.Value);
        //    }
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DemographicId, DistributionDate, GaveInfantFormula, PantryLocationId")] Distribution model)
        {
            var pantries = GetPantriesBasedOnRole();
            AddSelectLists(pantries);

            if (!ModelState.IsValid) return View();

            var newDistro = new Distribution
            {
                DemographicId = model.DemographicId,
                DistributionDate = model.DistributionDate,
                GaveInfantFormula = model.GaveInfantFormula,
                PantryLocationId = model.PantryLocationId,
                BagCount = 1
            };

            _context.Distribution.Add( newDistro );
            await _context.SaveChangesAsync();
            var tempData = _tempDataFactory.GetTempData(this.HttpContext);
            tempData.Add("SuccessMessage", "The distribution was successfully saved.");

            return RedirectToAction("Find", "Demographics");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Distribution distribution = await _context.Distribution.SingleOrDefaultAsync(d => d.DistributionId == id);
            if (distribution == null)
            {
                return NotFound();
            }
            ViewBag.DemographicId = new SelectList(_context.Demographic, "DemographicId", "LastName", distribution.DemographicId);
            return View(distribution);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("DistributionId,DemographicId,DistributionDate,BagCount,GaveInfantFormula")] Distribution distribution)
        {
            if (ModelState.IsValid)
            {
                _context.Entry(distribution).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.DemographicId = new SelectList(_context.Demographic, "DemographicId", "LastName", distribution.DemographicId);
            return View(distribution);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Distribution distribution = await _context.Distribution.SingleOrDefaultAsync(d => d.DistributionId == id);
            if (distribution == null)
            {
                return NotFound();
            }
            return View(distribution);
        }

        // POST: Distributions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Distribution distribution = await _context.Distribution.SingleOrDefaultAsync(d => d.DistributionId == id);
            _context.Distribution.Remove(distribution);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private void AddSelectLists(IQueryable pantries)
        {
            ViewData["PantryLocationId"] = new SelectList(pantries, "LookupValueId", "Description");
        }
    }
}
