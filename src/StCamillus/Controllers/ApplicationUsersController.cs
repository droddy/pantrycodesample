using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StCamillus.Data;
using StCamillus.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace StCamillus.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "User Admin")]
    public class ApplicationUsersController : Controller
    {
        private const string UserAdminRole = "User Admin";

        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public ApplicationUsersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: ApplicationUsers
        public async Task<IActionResult> Index()
        {
            var models = new List<ApplicationUsersIndexViewModel>();
            var users = await _context.ApplicationUser.ToListAsync();
            foreach (var user in users)
            {
                models.Add(new ApplicationUsersIndexViewModel(_userManager) { ApplicationUser = user });
            }
            return View(models);
        }

        // GET: ApplicationUsers/DisableLock/5s
        public async Task<IActionResult> DisableLock(string id)
        {
            var applicationUser = await _context.ApplicationUser.SingleAsync(u => u.Id == id);
            var result = await _userManager.SetLockoutEnabledAsync(applicationUser, false);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> ChangeUserRole(string id, string direction, string role)
        {
            var applicationUser = await _context.ApplicationUser.SingleAsync(u => u.Id == id);
            switch (direction)
            {
                case "Enable": { await _userManager.AddToRoleAsync(applicationUser, role); break; }
                case "Disable": { await _userManager.RemoveFromRoleAsync(applicationUser, role); break; }
            }
            return RedirectToAction("Index");
        }

        // GET: ApplicationUsers/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicationUser = await _context.ApplicationUser.SingleOrDefaultAsync(m => m.Id == id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return View(applicationUser);
        }

        // POST: ApplicationUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var applicationUser = await _context.ApplicationUser.SingleOrDefaultAsync(m => m.Id == id);
            _context.ApplicationUser.Remove(applicationUser);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet, ActionName("firstrun")]
        [AllowAnonymous]
        public async Task<IActionResult> CreateRolesAndAdminUsers()
        {
            await EnsureRolesCreated();
            await EnsureUserAdminUsersCreated();
            return Ok("Roles and Admins created");
        }

        private bool ApplicationUserExists(string id)
        {
            return _context.ApplicationUser.Any(e => e.Id == id);
        }
        private async Task EnsureUserAdminUsersCreated( )
        {
            var users = new List<ApplicationUser>
            {
                new ApplicationUser { Email = "sha57@verizon.net"},
                new ApplicationUser { Email = "david.droddy@gmail.com"},
                new ApplicationUser { Email = "yaojen.lu@gmail.com"}
            };

            foreach ( var user in users )
            {
                user.UserName = user.Email;
                var adminUser = await _userManager.FindByNameAsync( user.UserName );

                if ( adminUser == null )
                {
                    var result = await _userManager.CreateAsync( user, "***" );

                    adminUser = await _userManager.FindByNameAsync( user.UserName );
                    if ( adminUser != null )
                    {
                        if ( await _roleManager.RoleExistsAsync( UserAdminRole ) &&
                            ! await _userManager.IsInRoleAsync( adminUser, UserAdminRole ) )
                        {
                            await _userManager.AddToRoleAsync( adminUser, UserAdminRole );
                        }
                    }
                }
            }
        }
        private async Task EnsureRolesCreated( )
        {
            var roles = new List<IdentityRole>
            {
                new IdentityRole {Name = UserAdminRole},
                new IdentityRole {Name = "Standard User"},
                new IdentityRole {Name = "Silver Spring"},
                new IdentityRole {Name = "Langley Park"}
            };

            foreach ( var role in roles )
            {
                var roleExists = await _roleManager.RoleExistsAsync( role.Name.ToUpper() );
                if ( !roleExists )
                {
                    var result = await _roleManager.CreateAsync( role );
                }
            }
        }

    }
}
