using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StCamillus.Models;
using Microsoft.AspNetCore.Authorization;
using System;

namespace StCamillus.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "User Admin, Silver Spring, Langley Park")]
    public class MannaPickupController : Controller
    {
        private readonly StCamillusContext _context;

        public MannaPickupController(StCamillusContext context)
        {
            _context = context;    
        }

        //// GET: MannaPickup
        //public async Task<IActionResult> Index()
        //{
        //    var stCamillusContext = _context.MannaPickup.Include(m => m.MannaData).Include(m => m.PickupLocation);
        //    return View(await stCamillusContext.ToListAsync());
        //}

        // GET: MannaPickup/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mannaPickup = await _context.MannaPickup.SingleOrDefaultAsync(m => m.MannaPickupId == id);
            if (mannaPickup == null)
            {
                return NotFound();
            }

            return View(mannaPickup);
        }

        // GET: MannaPickup/Create/5 
        //id is the MannaDataId the new pickup should be associated with
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mannaPickup = new MannaPickup { MannaDataId = id.Value };
            var lastPickup = _context.MannaPickup.OrderByDescending( mp => mp.PickupDate ).FirstOrDefault( p => p.MannaDataId == id );
            ViewBag.LatestPickup = lastPickup == null ? "" : lastPickup.PickupDate.Value.ToString("MM/dd/yyyy");
            AddSelectLists();
            return View(mannaPickup);
        }

        // POST: MannaPickup/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MannaDataId,Notes,PickupDate,PickupLocationId")] MannaPickup mannaPickup)
        {
            if (ModelState.IsValid)
            {
                _context.Add(mannaPickup);
                await _context.SaveChangesAsync();
                return RedirectToAction("Edit", "MannaData", new { id = mannaPickup.MannaDataId });
            }
            AddSelectLists();
            return View(mannaPickup);
        }

        // GET: MannaPickup/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mannaPickup = await _context.MannaPickup.SingleOrDefaultAsync(m => m.MannaPickupId == id);
            if (mannaPickup == null)
            {
                return NotFound();
            }
            ViewData["MannaDataId"] = mannaPickup.MannaDataId;
            AddSelectLists();
            return View(mannaPickup);
        }

        // POST: MannaPickup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MannaPickupId,MannaDataId,Notes,PickupDate,PickupLocationId")] MannaPickup mannaPickup)
        {
            if (id != mannaPickup.MannaPickupId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(mannaPickup);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MannaPickupExists(mannaPickup.MannaPickupId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Edit", "MannaData", new { id = mannaPickup.MannaDataId });
            }
            ViewData["MannaDataId"] = mannaPickup.MannaDataId;
            AddSelectLists();
            return View(mannaPickup);
        }

        // GET: MannaPickup/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mannaPickup = await _context.MannaPickup.SingleOrDefaultAsync(m => m.MannaPickupId == id);
            if (mannaPickup == null)
            {
                return NotFound();
            }

            return View(mannaPickup);
        }

        // POST: MannaPickup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var mannaPickup = await _context.MannaPickup.SingleOrDefaultAsync(m => m.MannaPickupId == id);
            _context.MannaPickup.Remove(mannaPickup);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool MannaPickupExists(int id)
        {
            return _context.MannaPickup.Any(e => e.MannaPickupId == id);
        }

        private void AddSelectLists()
        {
            ViewData["PickupLocations"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(15) && l.Visible.Equals(true)), "LookupValueId", "Description");
        }
    }
}
