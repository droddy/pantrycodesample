using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using StCamillus.Models;
using Microsoft.AspNetCore.Authorization;

namespace StCamillus.Controllers
{
    [RequireHttps]
    [Authorize(Roles = "User Admin, Silver Spring, Langley Park")]
    public class MannaDataController : Controller
    {
        private readonly StCamillusContext _context;

        public MannaDataController(StCamillusContext context)
        {
            _context = context;    
        }

        public IActionResult AddEditByDemographicId( int? id )
        {
            if ( id == null )
            {
                return NotFound();
            }

            var mannaData = _context.MannaData.FirstOrDefault( md => md.DemographicId == id );

            int? mannaDataId = mannaData?.MannaDataId;

            return mannaDataId == null ? Create( id ).Result : RedirectToAction( "Edit", new { id = mannaDataId } );
        }

        // GET: MannaData
        //public async Task<IActionResult> Index()
        //{
        //    var stCamillusContext = _context.MannaData.Include(m => m.BabyFoodStage).Include(m => m.Demographic).Include(m => m.EmploymentStatus).Include(m => m.Gender).Include(m => m.Race).Include(m => m.Transportation);
        //    return View(await stCamillusContext.ToListAsync());
        //}

        // GET: MannaData/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var mannaData = await _context.MannaData.SingleOrDefaultAsync(m => m.MannaDataId == id);
            if (mannaData == null)
            {
                return NotFound();
            }

            return View(mannaData);
        }

        // GET: MannaData/Create/5
        //id is the Demographic ID this mannadata should be linked to
        private async Task<IActionResult> Create(int? id)
        {
            ViewBag.ChangesMade = false;
            if (id == null)
            {
                return NotFound();
            }

            if (!_context.Demographic.Any(d => d.DemographicId == id))
            {
                return NotFound();
            }

            var mannaData = new MannaData { DemographicId = id.Value };
            _context.Add(mannaData);
            await _context.SaveChangesAsync();
            var newId = _context.MannaData.FirstOrDefaultAsync(m => m.DemographicId == id).Result.MannaDataId;
            return RedirectToAction("Edit", new { id = newId, newRecord = true });
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("BabyFoodStageId,DemographicId,EmailAddress,EmergencyContactName,EmergencyContactPhone,EmploymentStatusId,FoodStampDollars,FormulaType,GenderId,IsEmployed,IsReceivingEnergyAssistance,IsUsingMedicaid,MonthlyGrossDollars,NeedsDiabeticBox,NeedsVegetarianBox,NumberOfChildren,OtherDollars,PrimaryLanguage,RaceId,SocialSecurityDisabilityInsuranceDollars,SocialSecurityInsuranceDollars,Tanfdollars,TransportationId,UnemploymentDollars,YearlyGrossDollars")] MannaData mannaData)
        //{
        //    ViewBag.ChangesMade = false;
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(mannaData);
        //        await _context.SaveChangesAsync();

        //        var newMannaData = await _context.MannaData.FirstOrDefaultAsync(m => m.DemographicId == mannaData.DemographicId);

        //        return RedirectToAction("Edit", new { id = newMannaData.MannaDataId, newRecord = true });
        //    }
        //    AddSelectLists();
        //    return View(mannaData);
        //}

        // GET: MannaData/Edit/5
        public async Task<IActionResult> Edit(int? id, bool newRecord = false)
        {
            ViewBag.ChangesMade = false;
            if (id == null)
            {
                return NotFound();
            }

            var mannaData = await _context.MannaData.Where(m => m.MannaDataId == id).Include(m => m.Demographic).Include(m => m.Demographic.Ethnicity).Include(m => m.MannaPickups).FirstOrDefaultAsync();
            if (mannaData == null)
            {
                return NotFound();
            }

            ViewBag.NewMannaDataCreated = newRecord;
            AddEthnicityToViewModel(mannaData);
            AddPickupLocationsToViewModel(mannaData);
            AddSelectLists();
            return View(mannaData);
        }

        // POST: MannaData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit( int id, [Bind( "MannaDataId,BabyFoodStageId,DemographicId,EmailAddress,EmergencyContactName,EmergencyContactPhone,EmploymentStatusId,FoodStampDollars,FormulaType,GenderId,IsEmployed,IsReceivingEnergyAssistance,IsUsingMedicaid,MonthlyGrossDollars,NeedsDiabeticBox,NeedsVegetarianBox,NumberOfChildren,OtherDollars,PrimaryLanguage,RaceId,SocialSecurityDisabilityInsuranceDollars,SocialSecurityInsuranceDollars,Tanfdollars,TransportationId,UnemploymentDollars,YearlyGrossDollars" )] MannaData mannaData )
        {
            ViewBag.NewMannaDataCreated = false;
            ViewBag.ChangesMade = false;
            if ( id != mannaData.MannaDataId )
            {
                return NotFound();
            }

            if ( ModelState.IsValid )
            {
                try
                {
                    _context.Update( mannaData );
                    await _context.SaveChangesAsync();
                }
                catch ( DbUpdateConcurrencyException )
                {
                    if ( !MannaDataExists( mannaData.MannaDataId ) )
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                ViewBag.ChangesMade = true;
            }

            var dbMannaData = await _context.MannaData.Where( m => m.MannaDataId == id ).Include( m => m.Demographic ).Include( m => m.Demographic.Ethnicity ).Include( m => m.MannaPickups ).FirstOrDefaultAsync();
            if ( dbMannaData == null )
            {
                return NotFound();
            }

            mannaData.Demographic = dbMannaData.Demographic;
            AddEthnicityToViewModel( mannaData );

            AddSelectLists();
            return View( mannaData );
        }

        private void AddPickupLocationsToViewModel(MannaData mannaData)
        {
            foreach (var pickup in mannaData.MannaPickups)
            {
                pickup.PickupLocation = _context.LookupValue.FirstOrDefaultAsync(l => l.LookupValueId == pickup.PickupLocationId).Result;
            }
        }

        private void AddEthnicityToViewModel(MannaData mannaData)
        {
            const string hispanicLatino = "Hispanic/Latino";
            ViewData["Ethnicity"] = mannaData.Demographic.Ethnicity?.Description == "Hispanic"
                ? hispanicLatino
                : string.Format("Not {0}", hispanicLatino);
        }

        //// GET: MannaData/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var mannaData = await _context.MannaData.SingleOrDefaultAsync(m => m.MannaDataId == id);
        //    if (mannaData == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(mannaData);
        //}

        // POST: MannaData/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var mannaData = await _context.MannaData.SingleOrDefaultAsync(m => m.MannaDataId == id);
        //    _context.MannaData.Remove(mannaData);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        private bool MannaDataExists(int id)
        {
            return _context.MannaData.Any(e => e.MannaDataId == id);
        }

        private void AddSelectLists()
        {
            ViewData["BabyFoodStages"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(10)), "LookupValueId", "Description");
            ViewData["Genders"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(11)), "LookupValueId", "Description");
            ViewData["EmploymentStatuses"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(12)), "LookupValueId", "Description");
            ViewData["Races"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(13)), "LookupValueId", "Description");
            ViewData["ModesOfTransportation"] = new SelectList(_context.LookupValue.Where(l => l.LookupCategoryId.Equals(14)), "LookupValueId", "Description");
        }
    }
}
