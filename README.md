##PreRequisites
- Install Microsoft [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms)
- Install Microsoft [Visual Studio](https://docs.microsoft.com/en-us/visualstudio/install/install-visual-studio)
- Install Microsoft [SQL Server Data Tools](https://docs.microsoft.com/en-us/sql/ssdt/download-sql-server-data-tools-ssdt)
- Install GIT or [SourceTree](https://www.sourcetreeapp.com/)

##Before First Run
1. Create a DB on your local ((localdb)\MSSQLLocalDB) called StCamillus
1. [Clone](#markdown-header-clone-a-repository) this Repository
1. Edit appsettings.json and change the *DegaultConnection* value to match your local DB setup. See *appsettings.local.json* for an example. If you have trouble with the mvc app connecting to your local db, check out this article: https://stackoverflow.com/questions/41220595/auto-generated-connection-string-of-localdb-mssqllocaldb-is-not-working-in-mvc?rq=1
1. In *Startup.cs* uncomment the lines with 
	```
	EnsureRolesCreated(app);
	```
and
	```
	EnsureUserAdminUsersCreated(app);
	```
1. Build the solution

##First Run
1. Run the app
1. Stop running the app
1. In your DB you should now see all the tables needed for ASP Authentication and Role management. Also notice the users and password created via the *EnsureUserAdminUsersCreated(app)* function that you uncommented in the above step. Either add something new here and run the app again or use one of the accounts already created.
1. DO NOT COMMIT OR PUSH ANY OF THE CHANGES YOU MADE ABOVE
1. Go to the [Wiki](https://bitbucket.org/stcamillusfoodpantry/mvcwebapp/wiki/Home) to learn about data migration and all the other nitty gritty details about this app.

---

## Branching Strategy
1. Do all dev work on the dev branch or branches off of the dev branch
1. NEVER publish the dev branch to PROD
1. when it is time to publish, tag the highest commit in dev branch with an incremented version, merge dev to master from the tag commit, checkout master, publish master

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.
