--select everything into a working table for ease of movement
SELECT zDistro.ID AS BarcodeID, --weird but true
zDistro.[Date of Visit] AS DistributionDate,
CASE 
	WHEN zDistro.[Bags of Food/Bolsas de Comida] IS NULL THEN 0
	ELSE zDistro.[Bags of Food/Bolsas de Comida]
END 
AS BagCount,
zDistro.[Infant Foods/ Comida para bebes] AS GaveInfantFormula,
CASE
	WHEN zDistro.LaptopNo = 0 THEN 26
	WHEN zDistro.LaptopNo = 1 THEN 25 
	WHEN zDistro.LaptopNo = 2 THEN 25 
	WHEN zDistro.LaptopNo = 3 THEN 25 
	WHEN zDistro.LaptopNo = 4 THEN 25 
	WHEN zDistro.LaptopNo = 5 THEN 25 
	WHEN zDistro.LaptopNo = 6 THEN 25
	WHEN zDistro.LaptopNo IS NULL THEN 26
END AS PantryLocationID
INTO #tmpDistroConversion
FROM [PantryAccessImport].dbo.Distribution AS zDistro
WHERE 
--ignore the 50 or so records that can't be mapped to an existing Demographic
EXISTS (select Demographic.BarcodeID FROM Demographic WHERE zDistro.ID = Demographic.BarcodeID)

--insert from the working table
--because, for some reason, the above query kekpt throwing foreign key constraint errors
INSERT INTO [Distribution] 
(DemographicID, DistributionDate, BagCount, GaveInfantFormula, PantryLocationID)
SELECT d.DemographicID, DistributionDate, BagCount, GaveInfantFormula, PantryLocationID 
FROM #tmpDistroConversion tmp
inner join Demographic d ON  d.BarcodeId = tmp.BarcodeID
WHERE EXISTS (select Demographic.BarcodeID FROM Demographic WHERE tmp.BarcodeID = Demographic.BarcodeID)
