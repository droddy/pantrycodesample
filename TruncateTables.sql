ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [FK_MannaData_LookupValue_TransportationID]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [FK_MannaData_LookupValue_RaceID]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [FK_MannaData_LookupValue_GenderID]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [FK_MannaData_LookupValue_EmploymentStatusID]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [FK_MannaData_LookupValue_BabyFoodStageID]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [FK_MannaData_Demographic_DemographicID]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [DF_MannaData_IsReceivingEnergyAssistance]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [DF_MannaData_IsUsingMedicaid]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [DF_MannaData_IsEmployed]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [DF_MannaData_NeedsVegetarianBox]
GO

ALTER TABLE [dbo].[MannaData] DROP CONSTRAINT [DF_MannaData_NeedsDiabeticBox]
GO

ALTER TABLE [dbo].[MannaPickup] DROP CONSTRAINT [FK_MannaPickup_MannaData]
GO

ALTER TABLE [dbo].[MannaPickup] DROP CONSTRAINT [FK_MannaPickup_LookupValue]
GO

ALTER TABLE [dbo].[Distribution] DROP CONSTRAINT [FK_Distribution_LookupValue_PantryLocationID]
GO

ALTER TABLE [dbo].[Distribution] DROP CONSTRAINT [FK_Distribution_Demographic_DemographicID]
GO

ALTER TABLE [dbo].[Demographic] DROP CONSTRAINT [FK_Demographic_LookupValue_StateID]
GO

ALTER TABLE [dbo].[Demographic] DROP CONSTRAINT [FK_Demographic_LookupValue_OriginCountryID]
GO

ALTER TABLE [dbo].[Demographic] DROP CONSTRAINT [FK_Demographic_LookupValue_MarriageStatusID]
GO

ALTER TABLE [dbo].[Demographic] DROP CONSTRAINT [FK_Demographic_LookupValue_EthnicityID]
GO

ALTER TABLE [dbo].[Demographic] DROP CONSTRAINT [FK_Demographic_LookupValue_CountyID]
GO

ALTER TABLE [dbo].[Demographic] DROP CONSTRAINT [FK_Demographic_LookupValue_BiWeeklyIncomeID]
GO

ALTER TABLE [dbo].[DemographicMultiValue] DROP CONSTRAINT [FK_DemographicMultiValue_LookupValue_LookupValueID]
GO

ALTER TABLE [dbo].[DemographicMultiValue] DROP CONSTRAINT [FK_DemographicMultiValue_Demographic_DemographicID]
GO

truncate table MannaData
truncate table MannaPickup
truncate table Distribution
truncate table Demographic
truncate table DemographicMultiValue

ALTER TABLE [dbo].[DemographicMultiValue]  WITH CHECK ADD  CONSTRAINT [FK_DemographicMultiValue_Demographic_DemographicID] FOREIGN KEY([DemographicID])
REFERENCES [dbo].[Demographic] ([DemographicID])
GO

ALTER TABLE [dbo].[DemographicMultiValue] CHECK CONSTRAINT [FK_DemographicMultiValue_Demographic_DemographicID]
GO

ALTER TABLE [dbo].[DemographicMultiValue]  WITH CHECK ADD  CONSTRAINT [FK_DemographicMultiValue_LookupValue_LookupValueID] FOREIGN KEY([LookupValueID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[DemographicMultiValue] CHECK CONSTRAINT [FK_DemographicMultiValue_LookupValue_LookupValueID]
GO

ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_BiWeeklyIncomeID] FOREIGN KEY([BiWeeklyIncomeID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_BiWeeklyIncomeID]
GO

ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_CountyID] FOREIGN KEY([CountyID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_CountyID]
GO

ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_EthnicityID] FOREIGN KEY([EthnicityID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_EthnicityID]
GO

ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_MarriageStatusID] FOREIGN KEY([MarriageStatusID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_MarriageStatusID]
GO

ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_OriginCountryID] FOREIGN KEY([OriginCountryID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_OriginCountryID]
GO

ALTER TABLE [dbo].[Demographic]  WITH CHECK ADD  CONSTRAINT [FK_Demographic_LookupValue_StateID] FOREIGN KEY([StateID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Demographic] CHECK CONSTRAINT [FK_Demographic_LookupValue_StateID]
GO

ALTER TABLE [dbo].[Distribution]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_Demographic_DemographicID] FOREIGN KEY([DemographicID])
REFERENCES [dbo].[Demographic] ([DemographicID])
GO

ALTER TABLE [dbo].[Distribution] CHECK CONSTRAINT [FK_Distribution_Demographic_DemographicID]
GO

ALTER TABLE [dbo].[Distribution]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_LookupValue_PantryLocationID] FOREIGN KEY([PantryLocationID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[Distribution] CHECK CONSTRAINT [FK_Distribution_LookupValue_PantryLocationID]
GO

ALTER TABLE [dbo].[MannaPickup]  WITH CHECK ADD  CONSTRAINT [FK_MannaPickup_LookupValue] FOREIGN KEY([PickupLocationId])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[MannaPickup] CHECK CONSTRAINT [FK_MannaPickup_LookupValue]
GO

ALTER TABLE [dbo].[MannaPickup]  WITH CHECK ADD  CONSTRAINT [FK_MannaPickup_MannaData] FOREIGN KEY([MannaDataId])
REFERENCES [dbo].[MannaData] ([MannaDataID])
GO

ALTER TABLE [dbo].[MannaPickup] CHECK CONSTRAINT [FK_MannaPickup_MannaData]
GO

ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_NeedsDiabeticBox]  DEFAULT ((0)) FOR [NeedsDiabeticBox]
GO

ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_NeedsVegetarianBox]  DEFAULT ((0)) FOR [NeedsVegetarianBox]
GO

ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_IsEmployed]  DEFAULT ((0)) FOR [IsEmployed]
GO

ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_IsUsingMedicaid]  DEFAULT ((0)) FOR [IsUsingMedicaid]
GO

ALTER TABLE [dbo].[MannaData] ADD  CONSTRAINT [DF_MannaData_IsReceivingEnergyAssistance]  DEFAULT ((0)) FOR [IsReceivingEnergyAssistance]
GO

ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_Demographic_DemographicID] FOREIGN KEY([DemographicID])
REFERENCES [dbo].[Demographic] ([DemographicID])
GO

ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_Demographic_DemographicID]
GO

ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_BabyFoodStageID] FOREIGN KEY([BabyFoodStageID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_BabyFoodStageID]
GO

ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_EmploymentStatusID] FOREIGN KEY([EmploymentStatusID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_EmploymentStatusID]
GO

ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_GenderID] FOREIGN KEY([GenderID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_GenderID]
GO

ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_RaceID] FOREIGN KEY([RaceID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_RaceID]
GO

ALTER TABLE [dbo].[MannaData]  WITH CHECK ADD  CONSTRAINT [FK_MannaData_LookupValue_TransportationID] FOREIGN KEY([TransportationID])
REFERENCES [dbo].[LookupValue] ([LookupValueID])
GO

ALTER TABLE [dbo].[MannaData] CHECK CONSTRAINT [FK_MannaData_LookupValue_TransportationID]
GO